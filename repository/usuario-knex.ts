import * as knex from 'knex'
import * as pg from 'pg'

import { Usuario, Endereco, Telefone } from '../entity/usuario'

const config: pg.PoolConfig = {
    host: 'localhost',
    database: 'servidor-simulacao',
    password: '123456'
}

const pool = new pg.Pool(config)
const from = knex({ client: 'pg', pool }).table

export class UsuarioRepositoryKnex {
    async listar(): Promise<Usuario[]> {
        const sql = from('usuario').toString()
        const resultado = await pool.query(sql)
        const usuarios: Usuario[] = resultado.rows
            .map(usuario => {
                return {
                    id: usuario.id,
                    nome: usuario.nome,
                    email: usuario.email,
                    password: usuario.password,
                    telefones: [],
                    enderecos: []
                }
            })
        for (const usuario of usuarios) {
            usuario.telefones = await this.telefones(usuario.id)
            usuario.enderecos = await this.enderecos(usuario.id)
        }
        return usuarios
    }

    private async telefones(usuarioId: number): Promise<Telefone[]> {
        const sql = from('usuario_telefone')
            .where({ usuario_id: usuarioId })
            .toString()
        const resultado = await pool.query(sql)
        return resultado.rows
            .map(usuarioTelefone => {
                return { numero: usuarioTelefone.numero }
            })
    }

    private async enderecos(usuarioId: number): Promise<Endereco[]> {
        const sql = from('usuario_endereco')
            .where({ usuario_id: usuarioId })
            .toString()
        const resultado = await pool.query(sql)

        const ids = resultado.rows.map(usuarioEndereco => {
            return usuarioEndereco.endereco_id
        })

        const enderecosSql = from('endereco')
            .whereIn('id', ids)
            .toString()
        const enderecos = await pool.query(enderecosSql)
        
        return enderecos.rows
            .map(endereco => {
                return {
                    id: endereco.id,
                    rua: endereco.rua,
                    numeroResidencia: endereco.numeroResidencia,
                    bairro: endereco.bairro,
                    cidade: endereco.cidade,
                    estado: endereco.estado
                }
            })
    }
}