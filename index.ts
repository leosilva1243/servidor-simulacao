import * as express from 'express'

import { usuarioRouter } from './router/usuario';
import { UsuarioRepositoryKnex } from './repository/usuario-knex'
import { UsuarioRepositorySequelize } from './repository/usuario-sequelize';

const app = express()
const usuarioRepository = new UsuarioRepositoryKnex()

app.use('/usuarios', usuarioRouter(usuarioRepository))

app.listen(8080, () => {
    console.log('escutando na porta 8080')
})