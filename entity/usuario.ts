
export interface Endereco {
    id: number
    rua: string
    numeroResidencia: string
    bairro: string
    cidade: string
    estado: string
}

export interface Telefone {
    numero: string
}

export interface Usuario {
    id: number
    nome: string
    email: string
    password: string
    telefones: Telefone[]
    enderecos: Endereco[]
}