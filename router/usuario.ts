import { Router } from 'express'

import { UsuarioRepositoryKnex } from '../repository/usuario-knex'
import { UsuarioRepositorySequelize } from '../repository/usuario-sequelize'

export function usuarioRouter(
    repository: UsuarioRepositoryKnex | UsuarioRepositorySequelize
) {
    const router = Router()

    router.get('/', async (request, response) => {
        const usuarios = await repository.listar()
        response.json(usuarios)
    })

    return router
}